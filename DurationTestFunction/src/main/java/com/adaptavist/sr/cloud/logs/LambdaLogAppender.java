package com.adaptavist.sr.cloud.logs;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonProperty;
//import org.apache.log4j.AppenderSkeleton;
//
//import javax.annotation.Nullable;
//import javax.annotation.ParametersAreNonnullByDefault;
//import javax.annotation.concurrent.Immutable;
//import javax.annotation.concurrent.NotThreadSafe;
//import java.util.*;
//import java.util.concurrent.ConcurrentLinkedQueue;
//
//@SuppressWarnings("CloseWithoutCloseable")
//public class LambdaLogAppender extends AppenderSkeleton {
//
//    private final Queue<LambdaLog> logs;
//
//    public LambdaLogAppender() {
//        this.logs = new ConcurrentLinkedQueue<>();
//    }
//
//    public List<LambdaLog> getLogs() {
//        return new ArrayList<>(logs);
//    }
//
//    protected void append(LoggingEvent event) {
//        logs.add(ImmutableLambdaLog.of(
//                event.getLevel().toInt(),
//                event.getRenderedMessage(),
//                event.timeStamp
//        ));
//    }
//
//    public void close() {
//        logs.clear();
//    }
//
//    public boolean requiresLayout() {
//        return false;
//    }
//
//}
//
//interface LambdaLog {
//
//    int level();
//
//    @Nullable
//    String message();
//
//    Long timestamp();  // in milliseconds
//
//    @Nullable
//    Map<String, Object> mdc();
//}
//
///**
// * Immutable implementation of {@link LambdaLog}.
// * <p>
// * Use the builder to create immutable instances:
// * {@code ImmutableLambdaLog.builder()}.
// * Use the static factory method to create immutable instances:
// * {@code ImmutableLambdaLog.of()}.
// */
//@SuppressWarnings({"all"})
//@ParametersAreNonnullByDefault
//@javax.annotation.processing.Generated("org.immutables.processor.ProxyProcessor")
//@Immutable
//@JsonIgnoreProperties(ignoreUnknown = true)
//class ImmutableLambdaLog implements LambdaLog {
//    private final int level;
//    private final @Nullable
//    String message;
//    private final Long timestamp;
//    private final @Nullable
//    Map<String, Object> mdc;
//
//    private ImmutableLambdaLog(
//            int level,
//            @Nullable String message,
//            Long timestamp,
//            @Nullable Map<String, ? extends Object> mdc) {
//        this.level = level;
//        this.message = message;
//        this.timestamp = Objects.requireNonNull(timestamp, "timestamp");
//        this.mdc = mdc == null ? null : createUnmodifiableMap(true, false, mdc);
//    }
//
//    private ImmutableLambdaLog(
//            ImmutableLambdaLog original,
//            int level,
//            @Nullable String message,
//            Long timestamp,
//            @Nullable Map<String, Object> mdc) {
//        this.level = level;
//        this.message = message;
//        this.timestamp = timestamp;
//        this.mdc = mdc;
//    }
//
//    /**
//     * @return The value of the {@code level} attribute
//     */
//    @JsonProperty("level")
//    @Override
//    public int level() {
//        return level;
//    }
//
//    /**
//     * @return The value of the {@code message} attribute
//     */
//    @JsonProperty("message")
//    @Override
//    public @Nullable
//    String message() {
//        return message;
//    }
//
//    /**
//     * @return The value of the {@code timestamp} attribute
//     */
//    @JsonProperty("timestamp")
//    @Override
//    public Long timestamp() {
//        return timestamp;
//    }
//
//    /**
//     * @return The value of the {@code mdc} attribute
//     */
//    @JsonProperty("mdc")
//    @Override
//    public @Nullable
//    Map<String, Object> mdc() {
//        return mdc;
//    }
//
//    /**
//     * Copy the current immutable object by setting a value for the {@link LambdaLog#level() level} attribute.
//     * A value equality check is used to prevent copying of the same value by returning {@code this}.
//     *
//     * @param value A new value for level
//     * @return A modified copy of the {@code this} object
//     */
//    public final ImmutableLambdaLog withLevel(int value) {
//        if (this.level == value) return this;
//        return new ImmutableLambdaLog(this, value, this.message, this.timestamp, this.mdc);
//    }
//
//    /**
//     * Copy the current immutable object by setting a value for the {@link LambdaLog#message() message} attribute.
//     * An equals check used to prevent copying of the same value by returning {@code this}.
//     *
//     * @param value A new value for message (can be {@code null})
//     * @return A modified copy of the {@code this} object
//     */
//    public final ImmutableLambdaLog withMessage(@Nullable String value) {
//        if (Objects.equals(this.message, value)) return this;
//        return new ImmutableLambdaLog(this, this.level, value, this.timestamp, this.mdc);
//    }
//
//    /**
//     * Copy the current immutable object by setting a value for the {@link LambdaLog#timestamp() timestamp} attribute.
//     * An equals check used to prevent copying of the same value by returning {@code this}.
//     *
//     * @param value A new value for timestamp
//     * @return A modified copy of the {@code this} object
//     */
//    public final ImmutableLambdaLog withTimestamp(Long value) {
//        Long newValue = Objects.requireNonNull(value, "timestamp");
//        if (this.timestamp.equals(newValue)) return this;
//        return new ImmutableLambdaLog(this, this.level, this.message, newValue, this.mdc);
//    }
//
//    /**
//     * Copy the current immutable object by replacing the {@link LambdaLog#mdc() mdc} map with the specified map.
//     * Nulls are not permitted as keys or values.
//     * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
//     *
//     * @param entries The entries to be added to the mdc map
//     * @return A modified copy of {@code this} object
//     */
//    public final ImmutableLambdaLog withMdc(@Nullable Map<String, ? extends Object> entries) {
//        if (this.mdc == entries) return this;
//        @Nullable Map<String, Object> newValue = entries == null ? null : createUnmodifiableMap(true, false, entries);
//        return new ImmutableLambdaLog(this, this.level, this.message, this.timestamp, newValue);
//    }
//
//    /**
//     * This instance is equal to all instances of {@code ImmutableLambdaLog} that have equal attribute values.
//     *
//     * @return {@code true} if {@code this} is equal to {@code another} instance
//     */
//    @Override
//    public boolean equals(@Nullable Object another) {
//        if (this == another) return true;
//        return another instanceof ImmutableLambdaLog
//                && equalTo((ImmutableLambdaLog) another);
//    }
//
//    private boolean equalTo(ImmutableLambdaLog another) {
//        return level == another.level
//                && Objects.equals(message, another.message)
//                && timestamp.equals(another.timestamp)
//                && Objects.equals(mdc, another.mdc);
//    }
//
//    /**
//     * Computes a hash code from attributes: {@code level}, {@code message}, {@code timestamp}, {@code mdc}.
//     *
//     * @return hashCode value
//     */
//    @Override
//    public int hashCode() {
//        int h = 5381;
//        h += (h << 5) + level;
//        h += (h << 5) + Objects.hashCode(message);
//        h += (h << 5) + timestamp.hashCode();
//        h += (h << 5) + Objects.hashCode(mdc);
//        return h;
//    }
//
//    /**
//     * Prints the immutable value {@code LambdaLog} with attribute values.
//     *
//     * @return A string representation of the value
//     */
//    @Override
//    public String toString() {
//        return "LambdaLog{"
//                + "level=" + level
//                + ", message=" + message
//                + ", timestamp=" + timestamp
//                + ", mdc=" + mdc
//                + "}";
//    }
//
//    /**
//     * Construct a new immutable {@code LambdaLog} instance.
//     *
//     * @param level     The value for the {@code level} attribute
//     * @param message   The value for the {@code message} attribute
//     * @param timestamp The value for the {@code timestamp} attribute
//     * @param mdc       The value for the {@code mdc} attribute
//     * @return An immutable LambdaLog instance
//     */
//    public static ImmutableLambdaLog of(int level, @Nullable String message, Long timestamp, @Nullable Map<String, ? extends Object> mdc) {
//        return new ImmutableLambdaLog(level, message, timestamp, mdc);
//    }
//
//    /**
//     * Creates an immutable copy of a {@link LambdaLog} value.
//     * Uses accessors to get values to initialize the new immutable instance.
//     * If an instance is already immutable, it is returned as is.
//     *
//     * @param instance The instance to copy
//     * @return A copied immutable LambdaLog instance
//     */
//    public static ImmutableLambdaLog copyOf(LambdaLog instance) {
//        if (instance instanceof ImmutableLambdaLog) {
//            return (ImmutableLambdaLog) instance;
//        }
//        return ImmutableLambdaLog.builder()
//                .from(instance)
//                .build();
//    }
//
//    /**
//     * Creates a builder for {@link ImmutableLambdaLog ImmutableLambdaLog}.
//     * <pre>
//     * ImmutableLambdaLog.builder()
//     *    .level(int) // required {@link LambdaLog#level() level}
//     *    .message(String | null) // nullable {@link LambdaLog#message() message}
//     *    .timestamp(Long) // required {@link LambdaLog#timestamp() timestamp}
//     *    .mdc(Map&amp;lt;String, Object&amp;gt; | null) // nullable {@link LambdaLog#mdc() mdc}
//     *    .build();
//     * </pre>
//     *
//     * @return A new ImmutableLambdaLog builder
//     */
//    public static ImmutableLambdaLog.Builder builder() {
//        return new ImmutableLambdaLog.Builder();
//    }
//
//    /**
//     * Builds instances of type {@link ImmutableLambdaLog ImmutableLambdaLog}.
//     * Initialize attributes and then invoke the {@link #build()} method to create an
//     * immutable instance.
//     * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
//     * but instead used immediately to create instances.</em>
//     */
//    @NotThreadSafe
//    @JsonIgnoreProperties(ignoreUnknown = true)
//    @JsonInclude(JsonInclude.Include.NON_ABSENT)
//    public static final class Builder {
//        private static final long INIT_BIT_LEVEL = 0x1L;
//        private static final long INIT_BIT_TIMESTAMP = 0x2L;
//        private long initBits = 0x3L;
//
//        private int level;
//        private @Nullable
//        String message;
//        private @Nullable
//        Long timestamp;
//        private Map<String, Object> mdc = null;
//
//        private Builder() {
//        }
//
//        /**
//         * Fill a builder with attribute values from the provided {@code LambdaLog} instance.
//         * Regular attribute values will be replaced with those from the given instance.
//         * Absent optional values will not replace present values.
//         * Collection elements and entries will be added, not replaced.
//         *
//         * @param instance The instance from which to copy values
//         * @return {@code this} builder for use in a chained invocation
//         */
//        public final Builder from(LambdaLog instance) {
//            Objects.requireNonNull(instance, "instance");
//            level(instance.level());
//            @Nullable String messageValue = instance.message();
//            if (messageValue != null) {
//                message(messageValue);
//            }
//            timestamp(instance.timestamp());
//            @Nullable Map<String, Object> mdcValue = instance.mdc();
//            if (mdcValue != null) {
//                putAllMdc(mdcValue);
//            }
//            return this;
//        }
//
//        /**
//         * Initializes the value for the {@link LambdaLog#level() level} attribute.
//         *
//         * @param level The value for level
//         * @return {@code this} builder for use in a chained invocation
//         */
//        @JsonProperty("level")
//        public final Builder level(int level) {
//            this.level = level;
//            initBits &= ~INIT_BIT_LEVEL;
//            return this;
//        }
//
//        /**
//         * Initializes the value for the {@link LambdaLog#message() message} attribute.
//         *
//         * @param message The value for message (can be {@code null})
//         * @return {@code this} builder for use in a chained invocation
//         */
//        @JsonProperty("message")
//        public final Builder message(@Nullable String message) {
//            this.message = message;
//            return this;
//        }
//
//        /**
//         * Initializes the value for the {@link LambdaLog#timestamp() timestamp} attribute.
//         *
//         * @param timestamp The value for timestamp
//         * @return {@code this} builder for use in a chained invocation
//         */
//        @JsonProperty("timestamp")
//        public final Builder timestamp(Long timestamp) {
//            this.timestamp = Objects.requireNonNull(timestamp, "timestamp");
//            initBits &= ~INIT_BIT_TIMESTAMP;
//            return this;
//        }
//
//        /**
//         * Put one entry to the {@link LambdaLog#mdc() mdc} map.
//         *
//         * @param key   The key in the mdc map
//         * @param value The associated value in the mdc map
//         * @return {@code this} builder for use in a chained invocation
//         */
//        public final Builder putMdc(String key, Object value) {
//            if (this.mdc == null) {
//                this.mdc = new LinkedHashMap<String, Object>();
//            }
//            this.mdc.put(
//                    Objects.requireNonNull(key, "mdc key"),
//                    Objects.requireNonNull(value, "mdc value"));
//            return this;
//        }
//
//        /**
//         * Put one entry to the {@link LambdaLog#mdc() mdc} map. Nulls are not permitted
//         *
//         * @param entry The key and value entry
//         * @return {@code this} builder for use in a chained invocation
//         */
//        public final Builder putMdc(Map.Entry<String, ? extends Object> entry) {
//            if (this.mdc == null) {
//                this.mdc = new LinkedHashMap<String, Object>();
//            }
//            String k = entry.getKey();
//            Object v = entry.getValue();
//            this.mdc.put(
//                    Objects.requireNonNull(k, "mdc key"),
//                    Objects.requireNonNull(v, "mdc value"));
//            return this;
//        }
//
//        /**
//         * Sets or replaces all mappings from the specified map as entries for the {@link LambdaLog#mdc() mdc} map. Nulls are not permitted as keys or values, but parameter itself can be null
//         *
//         * @param entries The entries that will be added to the mdc map
//         * @return {@code this} builder for use in a chained invocation
//         */
//        @JsonProperty("mdc")
//        public final Builder mdc(@Nullable Map<String, ? extends Object> entries) {
//            if (entries == null) {
//                this.mdc = null;
//                return this;
//            }
//            this.mdc = new LinkedHashMap<String, Object>();
//            return putAllMdc(entries);
//        }
//
//        /**
//         * Put all mappings from the specified map as entries to {@link LambdaLog#mdc() mdc} map. Nulls are not permitted
//         *
//         * @param entries The entries that will be added to the mdc map
//         * @return {@code this} builder for use in a chained invocation
//         */
//        public final Builder putAllMdc(Map<String, ? extends Object> entries) {
//            if (this.mdc == null) {
//                this.mdc = new LinkedHashMap<String, Object>();
//            }
//            for (Map.Entry<String, ? extends Object> e : entries.entrySet()) {
//                String k = e.getKey();
//                Object v = e.getValue();
//                this.mdc.put(
//                        Objects.requireNonNull(k, "mdc key"),
//                        Objects.requireNonNull(v, "mdc value"));
//            }
//            return this;
//        }
//
//        /**
//         * Builds a new {@link ImmutableLambdaLog ImmutableLambdaLog}.
//         *
//         * @return An immutable instance of LambdaLog
//         * @throws java.lang.IllegalStateException if any required attributes are missing
//         */
//        public ImmutableLambdaLog build() {
//            if (initBits != 0) {
//                throw new IllegalStateException(formatRequiredAttributesMessage());
//            }
//            return new ImmutableLambdaLog(
//                    null,
//                    level,
//                    message,
//                    timestamp,
//                    mdc == null ? null : createUnmodifiableMap(false, false, mdc));
//        }
//
//        private String formatRequiredAttributesMessage() {
//            List<String> attributes = new ArrayList<>();
//            if ((initBits & INIT_BIT_LEVEL) != 0) attributes.add("level");
//            if ((initBits & INIT_BIT_TIMESTAMP) != 0) attributes.add("timestamp");
//            return "Cannot build LambdaLog, some of required attributes are not set " + attributes;
//        }
//    }
//
//    private static <K, V> Map<K, V> createUnmodifiableMap(boolean checkNulls, boolean skipNulls, Map<? extends K, ? extends V> map) {
//        switch (map.size()) {
//            case 0:
//                return Collections.emptyMap();
//            case 1: {
//                Map.Entry<? extends K, ? extends V> e = map.entrySet().iterator().next();
//                K k = e.getKey();
//                V v = e.getValue();
//                if (checkNulls) {
//                    Objects.requireNonNull(k, "key");
//                    Objects.requireNonNull(v, "value");
//                }
//                if (skipNulls && (k == null || v == null)) {
//                    return Collections.emptyMap();
//                }
//                return Collections.singletonMap(k, v);
//            }
//            default: {
//                Map<K, V> linkedMap = new LinkedHashMap<>(map.size());
//                if (skipNulls || checkNulls) {
//                    for (Map.Entry<? extends K, ? extends V> e : map.entrySet()) {
//                        K k = e.getKey();
//                        V v = e.getValue();
//                        if (skipNulls) {
//                            if (k == null || v == null) continue;
//                        } else if (checkNulls) {
//                            Objects.requireNonNull(k, "key");
//                            Objects.requireNonNull(v, "value");
//                        }
//                        linkedMap.put(k, v);
//                    }
//                } else {
//                    linkedMap.putAll(map);
//                }
//                return Collections.unmodifiableMap(linkedMap);
//            }
//        }
//    }
//}
public class LambdaLogAppender {
}