package durationtest

import com.adaptavist.sr.cloud.SrCompilerUtils
import com.adaptavist.sr.cloud.TimeLimitedCodeBlock
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.json.MetricsModule
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.FilterProvider
import com.fasterxml.jackson.databind.ser.PropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import io.github.openunirest.http.Unirest
import io.github.openunirest.http.options.Options
import org.codehaus.groovy.control.CompilerConfiguration

import java.time.Duration
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Handler for requests to Lambda function.
 */
@Slf4j
@CompileStatic
class MainHandler implements RequestStreamHandler {

    static ObjectMapper objectMapper = new ObjectMapper();
    static ObjectWriter objectWriter
    static MetricRegistry metricRegistry = new MetricRegistry()

    static {
        objectMapper.registerModule(new SimpleModule("Custom Serializers")
                .addSerializer(GString, ToStringSerializer.instance))

        PropertyFilter theFilter = SimpleBeanPropertyFilter.serializeAllExcept("proxyTarget")
        FilterProvider filters = new SimpleFilterProvider()
                .addFilter("proxyTarget", (PropertyFilter) theFilter)
                .setDefaultFilter((PropertyFilter) theFilter)
        objectMapper.registerModule(new MetricsModule(TimeUnit.MILLISECONDS, TimeUnit.MILLISECONDS, true))
        objectWriter = objectMapper.writer(filters)
        objectMapper.setFilterProvider(filters)

        Unirest.setObjectMapperAndWriter(objectMapper, objectWriter)

        Unirest.setObjectMapperAndWriter(objectMapper, objectWriter)
        Options.init()
    }

    //run unirest here to see what happens

    @Override
    void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
        LambdaLogger logger = context.getLogger();

        String code = """
                            def i = 0
                            while (true) {
                                                        logger.println("Current Thread isInterrupted: " + Thread.currentThread().isInterrupted())
                                                        logger.println(Thread.currentThread().name + ' going to sleep ' + ++i)
                                                        sleep(7000)
                                                        logger.println(Thread.currentThread().name + " I'm awake " + i)
                                                        logger.println("Current Thread isInterrupted: " + Thread.currentThread().isInterrupted())
                                                    }
                        """
        try {
            //test with a sleep here
            TimeLimitedCodeBlock.runWithTimeout(
                    {
                        logger.println("Code block starts")

                        def proxyHeaders = [
                                (HttpHeaderNames.AUTHORIZATION)  : "JWT",
                                (HttpHeaderNames.ACCEPT)         : "HttpHeValues PPLICATION_JSON",
                                (HttpHeaderNames.ACCEPT_CHARSET) : "UTF_8",
                                (HttpHeaderNames.ACCEPT_ENCODING): "VALID_ENCODINGS",
                                (HttpHeaderNames.USER_AGENT)     : 'ScriptRunner Add-on / AWS Lambda'
                        ]

                        Unirest.setProxyHeaders(proxyHeaders)
                        Unirest.setLogger(log)
                        Unirest.setMetricRegistry(metricRegistry)


                        CompilerConfiguration configuration =
                                SrCompilerUtils.srCompilerConfiguration(['a': 'b'] as Map<String, Object>)

//                        configuration = new CompilerConfiguration()
                        GroovyShell shell = new GroovyShell(this.class.
                                classLoader, new Binding(["logger": logger]), configuration)

                        Script script = shell.parse(code)

                        script.setBinding(shell.getContext())
                        script.run()
                        logger.println("Code block completed")
                    } as GroovyCallable, Duration.ofSeconds(20))
        } catch (TimeoutException e) {
            logger.println("Main Handler " + e.getMessage())

        } catch (Exception) {
            logger.println("Main Handler caught a timeout exception")
        }
    }

    interface HttpHeaderNames {
        @Deprecated
        String CORRELATION_ID = "correlationId"; // Should use CorrelationId.HEADER_NAME instead
        String EXECUTION_ID = "executionId";

        String CLIENT_KEY = "clientKey";
        String BASE_URL = "baseUrl";
        String AUTHORIZATION = "Authorization";
        String ACCEPT_CHARSET = "accept-charset";
        String ACCEPT = "accept";
        String ACCEPT_ENCODING = "accept-encoding";
        String PROXY_UPSTREAM_TIMEOUT = "proxy-upstream-timeout";
        String USER_AGENT = "user-agent";
    }


}
