package com.adaptavist.sr.cloud;

import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ImportCustomizer;

import java.util.Map;

public interface SrCompilerUtils {
    static CompilerConfiguration srCompilerConfiguration() {
        CompilerConfiguration configuration = new CompilerConfiguration(CompilerConfiguration.DEFAULT);
        ImportCustomizer importCustomizer = new ImportCustomizer();
        importCustomizer.addStaticStars("io.github.openunirest.http.Unirest");
        importCustomizer.addStarImports("io.github.openunirest.http");
        configuration.addCompilationCustomizers(importCustomizer);
        return configuration;
    }

    static CompilerConfiguration srCompilerConfiguration(Map<String, Object> variables) {
        CompilerConfiguration config = srCompilerConfiguration();
        config.addCompilationCustomizers(new VariableCompilerCustomizer(variables));

        return config;
    }

//    static CompilerConfiguration srCompilerConfiguration(Duration timedInterruptLimit) {
//        CompilerConfiguration config = srCompilerConfiguration();
//
//        Map<String, Long> annotationParams = new HashMap<>();
//        annotationParams.put("value", timedInterruptLimit.getSeconds());
//        config.addCompilationCustomizers(new ASTTransformationCustomizer(annotationParams, TimedInterrupt.class));
//
//        return config;
//    }
//
//
//    static CompilerConfiguration srCompilerConfiguration() {
//        CompilerConfiguration configuration = new CompilerConfiguration(CompilerConfiguration.DEFAULT);
//        return configuration;
//    }

}
