package com.adaptavist.sr.cloud;

import groovy.lang.GroovyCallable;

import java.time.Duration;
import java.util.concurrent.*;

public class TimeLimitedCodeBlock {

    private TimeLimitedCodeBlock() {
    }

    public static <T> T runWithTimeout(GroovyCallable<T> callable, Duration timeout) throws Exception {
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<T> future = executor.submit(callable);

        try {
            return future.get(timeout.getSeconds(), TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            System.out.println(Thread.currentThread().getName() + " caught it inside the timed block");
            future.cancel(true);
            throw e;
        } catch (ExecutionException e) {
            Throwable t = e.getCause();
            if (t instanceof Error) {
                throw (Error) t;
            } else if (t instanceof Exception) {
                throw (Exception) t;
            } else {
                throw new IllegalStateException(t);
            }
        } finally {
            long l = System.currentTimeMillis();
            System.out.println(Thread.currentThread().getName() + " shut down start" + System.currentTimeMillis());
            executor.shutdown();
            System.out.println(Thread.currentThread().getName() + " shut down complete. took " + (System.currentTimeMillis() - l) + "ms");

        }
    }
}
