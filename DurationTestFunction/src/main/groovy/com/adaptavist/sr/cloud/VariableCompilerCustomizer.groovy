package com.adaptavist.sr.cloud

import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.classgen.GeneratorContext
import org.codehaus.groovy.control.CompilationFailedException
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.control.customizers.CompilationCustomizer

/**
 * Just holds a copy of the variable values for a given script's annotations that are
 * then set by the VariableValueTransformation
 */
class VariableCompilerCustomizer extends CompilationCustomizer {

    private final Map<String, Object> variables

    VariableCompilerCustomizer(Map<String, Object> variables) {
        super(CompilePhase.CANONICALIZATION)
        this.variables = variables
    }

    Map<String, Object> getVariables() {
        variables
    }

    @Override
    void call(SourceUnit sourceUnit,
              GeneratorContext generatorContext,
              ClassNode classNode) throws CompilationFailedException {
        // Do nothing
    }
}
