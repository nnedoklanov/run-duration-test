version = 0.1
[default]
[default.deploy]
[default.deploy.parameters]
stack_name = "duration-test"
s3_bucket = "aws-sam-cli-managed-default-samclisourcebucket-3yd6krcypipv"
s3_prefix = "duration-test"
region = "eu-west-2"
confirm_changeset = true
capabilities = "CAPABILITY_IAM"
